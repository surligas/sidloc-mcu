
# SIDLOC MCU

## Connector pinouts

### Pinout for J106 (RFFE):

| Pin number | Pin name | Pin net          |
|------------|----------|------------------|
| 1          | Pin\_1   | MCU/RFFE\_FSENSE |
| 2          | Pin\_2   | MCU/RFFE\_PSENSE |
| 3          | Pin\_3   | MCU/RFFE\_EN     |
| 4          | Pin\_4   | MCU/RFFE\_TX\_EN |
| 5          | Pin\_5   | GND              |
| MP         |          |                  |

### Pinout for J109 (Thermal Knifes):

| Pin number | Pin name | Pin net      |
|------------|----------|--------------|
| 1          | Pin\_1   | BAT+         |
| 2          | Pin\_2   | MCU/ANT\_DEP |
| 3          | Pin\_3   | MCU/ANT\_SEN |
| 4          | Pin\_4   | GND          |

### Pinout for J108 (JTAG):

| Pin number | Pin name | Pin net             |
|------------|----------|---------------------|
| 1          | Pin\_1   | MCU/SYS\_JTCK-SWCLK |
| 2          | Pin\_2   | MCU/SYS\_JTDI       |
| 3          | Pin\_3   | JTDO                |
| 4          | Pin\_4   | MCU/SYS\_JTRST      |
| 5          | Pin\_5   | MCU/SYS\_JTMS-SWDIO |
| 6          | Pin\_6   | GND                 |
| MP         |          |                     |

### Pinout for J104 (KILL):

| Pin number | Pin name | Pin net           |
|------------|----------|-------------------|
| 1          | Pin\_1   | Power/KILL\_NO\_1 |
| 2          | Pin\_2   | Power/KILL\_NO\_2 |
| MP         |          |                   |

### Pinout for J101 (BAT):

| Pin number | Pin name | Pin net |
|------------|----------|---------|
| 1          | Pin\_1   | TBat    |
| 2          | Pin\_2   | BAT-    |
| 3          | Pin\_3   | BAT+    |
| MP         |          |         |

### Pinout for J102 (VIN/TM):

| Pin number | Pin name | Pin net |
|------------|----------|---------|
| 1          | Pin\_1   | VIN     |
| 2          | Pin\_2   | GND     |
| 3          | Pin\_3   | TM      |
| 4          | Pin\_4   | TM\_R   |
| MP         |          |         |

